import os
import xacro
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import IncludeLaunchDescription, DeclareLaunchArgument, RegisterEventHandler, ExecuteProcess
from launch.event_handlers import OnProcessExit
from launch.substitutions import LaunchConfiguration, PathJoinSubstitution
from launch.conditions import IfCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch_ros.substitutions import FindPackageShare
from launch_ros.actions import Node

def generate_launch_description():

    # Package Directories
    # pkg_ros_gz_sim = get_package_share_directory('ros_gz_sim')
    pkg_movee_bringup = get_package_share_directory('movee_bringup')
    pkg_teleop_joy = get_package_share_directory('teleop_joy_cmd_vel')
    pkg_movee_description = get_package_share_directory('movee_description')
    pkg_movee_control = get_package_share_directory('movee_control')

    # Parse robot description from xacro
    robot_description_file =  os.path.join(pkg_movee_description, 'urdf/', 'movee.xacro')    
    robot_description_config = xacro.process_file(robot_description_file)
    robot_description = {"robot_description": robot_description_config.toxml()}

    # Launch configuration variables 
    rviz_config_file = LaunchConfiguration('rviz_config_file')
    use_rviz = LaunchConfiguration('use_rviz')
    use_joy_teleop = LaunchConfiguration('use_joy')
    use_movee_controller = LaunchConfiguration('use_movee_controller')
    use_robot_state_pub = LaunchConfiguration('use_robot_state_pub')

    #Declare Launch Arguments    
    declare_rviz_config_file_cmd = DeclareLaunchArgument(
        'rviz_config_file',
        default_value=os.path.join(pkg_movee_description, 'rviz', 'diffbot.rviz'),
        description='Full path to the RVIZ config file to use')
    declare_use_rviz_cmd = DeclareLaunchArgument(
        'use_rviz',
        default_value='True',
        description='Whether to start RVIZ')
    declare_use_joy_teleop = DeclareLaunchArgument(
        'use_joy',
        default_value='True',
        description='Determines if Joy Teleop nodes should be launched')
    declare_use_joint_animation = DeclareLaunchArgument(
        'use_movee_controller',
        default_value='True',
        description='Whether to start the move controller (publishing joint_states or forward_position) from MoveeCtrl topic and kinematics computation)')    
    declare_use_robot_state_pub_cmd = DeclareLaunchArgument(
        'use_robot_state_pub',
        default_value='False',
        description='Whether to start the robot state publisher')
    


    #Launch Controllers
    launch_position_publisher = IncludeLaunchDescription(
        PythonLaunchDescriptionSource([
                PathJoinSubstitution([
                    FindPackageShare('movee_control'),
                    'launch',
                    'movee_control.launch.py'
                ])
            ]),
            launch_arguments={'use_movee_ctrl_joint_state':'True',
                              'use_controllers_for_gz':'False'
            }.items(),
            condition=IfCondition(use_movee_controller)
    )

    
    launch_teleop_joy = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(pkg_teleop_joy, 'launch', 'movee_teleop.launch.py'),
        ),
        launch_arguments={'joy_config':'PS4',
                          'joy_device_id':'0',
                          'pub_topic':'/movee_ctrl_topic'
        }.items(),
        condition=IfCondition(use_joy_teleop)
    )     

    #Rviz
    rviz_cmd = Node(
        condition=IfCondition(use_rviz),
        package='rviz2',
        executable='rviz2',
        name='rviz2',
        arguments=['-d', rviz_config_file],
        output='screen')
    
    start_robot_state_publisher_cmd = Node(
        condition=IfCondition(use_robot_state_pub),
        package='robot_state_publisher',
        executable='robot_state_publisher',
        name='robot_state_publisher',
        output='screen',
        #parameters=[{'use_sim_time': use_sim_time}],
        #arguments=[urdf_file])
        parameters=[robot_description])
    
    #Static Transforms
    st_tf = Node(
            package='tf2_ros',            
            executable='static_transform_publisher',
            arguments= ["0", "0", "0", "0", "0", "0", "lidar_link", "lidar"]
        )
 

    # Create the launch description and populate
    ld = LaunchDescription()

    # Add any conditioned actions
    ld.add_action(declare_rviz_config_file_cmd)
    ld.add_action(declare_use_rviz_cmd)
    ld.add_action(declare_use_joy_teleop)
    ld.add_action(declare_use_robot_state_pub_cmd)
    #ld.add_action(st_tf)
    ld.add_action(declare_use_joint_animation)
    ld.add_action(start_robot_state_publisher_cmd)
    ld.add_action(rviz_cmd)    
    ld.add_action(launch_position_publisher)
    ld.add_action(launch_teleop_joy)
   
 

    return ld   
