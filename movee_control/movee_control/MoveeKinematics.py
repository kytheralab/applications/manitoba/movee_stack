####### Movee kinematics
import numpy as np
import math
from scipy.optimize import fsolve

class MoveeKinematics:
    
    
    def __init__(self, xRoot=0, yRoot=0):
        self.xRoot = xRoot
        self.yRoot = yRoot
        self.rwheel = 0.1
        self.offset_hip = 4.7124 ##The Solver was developped to work above 3.33 rad !!!
        self.compensation_knee = -np.pi
        self.compensation_conrod = -3*np.pi/2
        #Select max and min value for input angle + linearization value for table:
        hip_min_angle = -1.4
        hip_max_angle = -0.4
        nbr_steps = 200

        #Generate computation table:
        self.angle_table = self.compute_kinematics_table(hip_min_angle,hip_max_angle,nbr_steps)
        
        
        
    def compute_kinematics_table(self, theta_1_min = -1.38, theta_1_max = -0.4, linearization_steps=100):
        l0 = 0.08
        l1 = 0.273
        l2 = 0.04472
        l3 = 0.28
        l4x = 0.2685
        l4y = -0.01
        dy = 0.06337
        dx = -0.04883
        rwheel = 0.1

        # a1 = 3.9
        # a3 = 0

        Fbar = lambda a1,x: np.array([l1*np.sin(a1)+l2*np.sin(x[0]+a1)-l3*np.sin(x[1])-dy,
                                l1*np.cos(a1)+l2*np.cos(x[0] +a1)-l3*np.cos(x[1])-dx])
                
        
        theta_1_min+=self.offset_hip
        theta_1_max+=self.offset_hip

        angle1 = np.linspace(theta_1_min, theta_1_max, linearization_steps)  #Augment second argument to bring foot lower
        angle2 = np.zeros(len(angle1))
        angle3 = np.zeros(len(angle1))
        height = np.zeros(len(angle1))
        xsol = np.array([0, np.pi/4])

        for i in range(len(angle1)):
            xsol = fsolve(lambda x: Fbar(angle1[i], x), xsol)
            angle2[i] = xsol[0]
            angle3[i] = xsol[1]
            height[i] = l1*np.sin(angle1[i])-l4x*np.sin(angle2[i]+angle1[i])-rwheel

        ## Apply module 2PI on the results
        angle1 = angle1%(2*np.pi) #angle of the hip joint
        angle2 = angle2%(2*np.pi) #angle of the knee joint
        angle3 = angle3%(2*np.pi) #angle of the conrod joint (at hip)

        return [angle1, angle2, angle3, height]
    
    def extact_angle_from_height(self, height):
        table = self.angle_table
        i = np.absolute(table[3]-height).argmin() ##Find the index of the closest value in the np.array
        # print("i:"+str(i)+"a1:"+str(a1)+"angle:"+str(angle))
        aa1 = table[0][i]-self.offset_hip
        aa2 = table[1][i]+self.compensation_knee
        aa3 = table[2][i]+self.compensation_conrod
        h = table[3][i]
        return [aa1,aa2,aa3,h]    