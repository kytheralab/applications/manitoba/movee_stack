import rclpy
import sys
from rclpy.node import Node
from sensor_msgs.msg import JointState
from geometry_msgs.msg import Twist
from movee_communication.msg import MoveeCtrl 


from sensor_msgs.msg import JointState
from std_msgs.msg import Header, Float64MultiArray


from movee_control.MoveeKinematics import *

class MoveeJointStatePublisher(Node):

    def __init__(self):
        super().__init__('moveeJointStatePublisher')
        self.get_logger().info(f'MoveeJointStatePublisher Node created: {1}')  
        
        #Initiate Robot height:
        self.target_height = 0.45
        self.max_height = 0.61
        self.min_height=0.21        
        self.movee_kin = MoveeKinematics()
        # self.get_logger().info(f'Original table: \n{self.movee_kin.angle_table}')
        self.angles = [-0.83,1.64,-0.71,0.5]
                
        # Create subscription to MoveeCtrl
        self.subscription = self.create_subscription(MoveeCtrl,'movee_ctrl_topic', self.movee_ctrl_callback, 10)
        
        # Create a Twist publisher and send first instruction
        argv = sys.argv[1:] 
        publish_topic = argv[1]
        self.get_logger().info(f'Twist publish topic: {publish_topic}')  
        self.twist_publisher = self.create_publisher(Twist, publish_topic, 10)
        self.twist_cmd = Twist()
        self.publish_twist()
        
        #Create the ForwardCommandPublisher
        self.forward_command_publisher = self.create_publisher(Float64MultiArray, 'forward_position_controller/commands', 10)
        # self.joint_names = [
        #     'hip_pivot_right',
        #     'hip_pivot_left',
        #     'knee_pivot_right',
        #     'knee_pivot_left',
        #     'hip_pivot_conrod_right',
        #     'hip_pivot_conrod_left',
        #     'counterweight_slide'
        # ]
                
    def movee_ctrl_callback(self, msg):
        # Extract data from MoveeCtrl message
        self.x_linear_velocity = msg.x_linear_velocity
        self.z_angular_velocity = msg.z_angular_velocity
        self.target_height = msg.height
        self.lateral_tilt = msg.lateral_tilt
        self.counterweight_pos = msg.counterweight_tilt
        
        #Update the Twist message
        self.twist_cmd.linear.x = self.x_linear_velocity
        self.twist_cmd.angular.z = self.z_angular_velocity
        self.publish_twist()
        self.update_height()
        self.publish_forward_command()            
        
        # Log info (optional)
        # self.get_logger().info('Published JointState: height=%f, tilt=%f' % (height, lateral_tilt))
    
    def update_height(self):
        # self.get_logger().info(f'Target Height: {self.target_height}')       
        self.angles = self.movee_kin.extact_angle_from_height(-self.target_height)
        # self.get_logger().info(f'Publishing: {self.angles}')

        
    ## Publish the Twist message
    def publish_twist(self):
        #self.goal_positions = self.mvt       
        self.twist_publisher.publish(self.twist_cmd)
        # print(twist_msg)    

    def publish_forward_command(self):
        msg = Float64MultiArray()
        # Simulating some joint movements (replace with actual desired joint positions)
        #msg.data = [-0.83, -0.83, 1.64, 1.64, -0.71, -0.71, self.counterweight_pos]
        msg.data = [self.angles[0], self.angles[0], self.angles[1], self.angles[1], self.angles[2], self.angles[2], self.counterweight_pos]
        self.forward_command_publisher.publish(msg)
        #self.get_logger().info(f'Publishing: {dict(zip(self.joint_names, msg.data))}')           

def main(args=None):
    rclpy.init(args=args)
    node = MoveeJointStatePublisher()
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
