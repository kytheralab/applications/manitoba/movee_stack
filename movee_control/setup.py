import os
from glob import glob
from setuptools import setup
from setuptools import find_packages
package_name = 'movee_control'

setup(
    name=package_name,
    version='0.1.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
         # Include all launch files
        (os.path.join('share', package_name, 'launch'), glob('launch/*')),
        # Include model and simulation files
        (os.path.join('share', package_name, 'config'), glob('config/*')),
        # Include custom messages
        #(os.path.join('share', package_name, 'msg'), glob('msg/*')),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='luczia',
    maintainer_email='lucas.soubeyrand@kythera.fr',
    description='ROS2 movee control package',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
                'joint_animation_node = movee_control.joint_trajectory_publisher:main',             
                'joint_state_publisher_node = movee_control.joint_state_publisher:main',      
                'forward_position_command_publisher_node = movee_control.forward_position_command_publisher:main',      
        ],
    },
)
