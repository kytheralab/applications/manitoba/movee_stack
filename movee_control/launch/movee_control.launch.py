import os

from launch import LaunchDescription
from launch.substitutions import Command, FindExecutable, PathJoinSubstitution
from ament_index_python.packages import get_package_share_directory

from launch.conditions import IfCondition
from launch_ros.actions import Node
from launch.actions import ExecuteProcess, DeclareLaunchArgument,RegisterEventHandler, IncludeLaunchDescription
from launch_ros.substitutions import FindPackageShare
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration
from launch.event_handlers import OnProcessExit
from launch_ros.substitutions import FindPackageShare



def generate_launch_description():

    pkg_movee_control = get_package_share_directory('movee_control')
    pkg_movee_description = get_package_share_directory('movee_description')

    # Launch configurations variables 
    use_controllers_for_gz = LaunchConfiguration('use_controllers_for_gz')
    use_static_forward_position = LaunchConfiguration('use_static_forward_position')
    use_movee_ctrl_forward_position = LaunchConfiguration('use_movee_ctrl_forward_position')
    use_movee_ctrl_joint_state = LaunchConfiguration('use_movee_ctrl_joint_state')
  
     
    twist_pub_topic = LaunchConfiguration('twist_pub_topic')

    robot_controllers = os.path.join(pkg_movee_control, 'config/', 'movee_controller.yaml')
    robot_description = os.path.join(pkg_movee_description, 'urdf/', 'movee.xacro')  

    declare_use_use_controllers_for_gz = DeclareLaunchArgument(
        'use_controllers_for_gz',
        default_value='false',
        description='Whether to start the controller manager and joint_state_broadcaster Publisher')    

    declare_use_static_forward_position = DeclareLaunchArgument(
        'use_static_forward_position',
        default_value='false',
        description='Whether to start static forward position Publisher')    
    
    declare_use_movee_ctrl_forward_position = DeclareLaunchArgument(
        'use_movee_ctrl_forward_position',
        default_value='false',
        description='Whether to start static forward position Publisher')    
    
    declare_use_movee_ctrl_joint_state = DeclareLaunchArgument(
        'use_movee_ctrl_joint_state',
        default_value='false',
        description='Whether to start static forward position Publisher')    
    
    declare_twist_twist_pub_topic = DeclareLaunchArgument(
        'twist_pub_topic',
        default_value='/cmd_vel'
        ,description='Name of the Twist topic published by the controller')
    
    

    control_node = Node(
        package="controller_manager",
        executable="ros2_control_node",
        parameters=[robot_description, robot_controllers],
        output="both",
        condition=IfCondition(use_controllers_for_gz)
    )

    joint_state_broadcaster_spawner = Node(
        package="controller_manager",
        executable="spawner",
        arguments=["joint_state_broadcaster", "--controller-manager", "/controller_manager"],
        condition=IfCondition(use_controllers_for_gz)
    )

    forward_position_controller_spawner = Node(
        package="controller_manager",
        executable="spawner",
        arguments=["forward_position_controller", "--controller-manager", "/controller_manager"],
    )

    diff_base_controller_spawner = Node(
        package="controller_manager",
        executable="spawner",
        arguments=["diff_drive_base_controller", "--controller-manager", "/controller_manager"],        
    )
 
    # load_joint_state_broadcaster = ExecuteProcess(
	# 									cmd=['ros2', 'control', 'load_controller', '--set-state', 'active','joint_state_broadcaster'],
	# 									output='screen')
                                        
    # load_joint_trajectory_controller = ExecuteProcess( 
	# 									cmd=['ros2', 'control', 'load_controller', '--set-state', 'active', 'joint_trajectory_controller'], 
	# 									output='screen')

    # load_diff_drive_base_controller = ExecuteProcess(
    #                                     cmd=['ros2', 'control', 'load_controller', '--set-state', 'active',  'diff_drive_base_controller'],
    #                                     output='screen' )
 
    
    # Delay start of robot_controller after `joint_state_broadcaster`
    delay_forward_controller_spawner_after_joint_state_broadcaster_spawner = RegisterEventHandler(
        event_handler=OnProcessExit(
            target_action=joint_state_broadcaster_spawner,
            on_exit=[forward_position_controller_spawner],
        )
    ) 

    # Delay start of robot_controller after `joint_state_broadcaster`
    delay_diff_controller_spawner_after_joint_state_broadcaster_spawner = RegisterEventHandler(
        event_handler=OnProcessExit(
            target_action=joint_state_broadcaster_spawner,
            on_exit=[diff_base_controller_spawner],
        )
    ) 


    static_forward_position_publisher = IncludeLaunchDescription(
        PythonLaunchDescriptionSource([
                PathJoinSubstitution([
                    FindPackageShare('movee_control'),
                    'launch',
                    'static_forward_position_controller.launch.py'
                ])
            ]),
            launch_arguments={},
            
            condition=IfCondition(use_static_forward_position)
    )
    
    node_forward_position_publisher = Node(
            condition=IfCondition(use_movee_ctrl_forward_position),
            package='movee_control', 
            executable='forward_position_command_publisher_node',
            name='forward_position_command_publisher_node',             
            arguments=[
                    '-twist_pub_topic', LaunchConfiguration('twist_pub_topic'),                    
                    ],
            output='screen'
            )
    
    node_joint_state_publisher = Node(
                condition=IfCondition(use_movee_ctrl_joint_state),
                package='movee_control',
                executable='joint_state_publisher_node',
                name='joint_state_publisher_node',    
                arguments=[
                    '-twist_pub_topic', LaunchConfiguration('twist_pub_topic'),                    
                    ],    
                output='screen'
            )

 

    ld = LaunchDescription()

    # ld.action(joint_state_broadcaster_spawner)
    # ld.action(delay_robot_controller_spawner_after_joint_state_broadcaster_spawner)
    ld.add_action(declare_use_use_controllers_for_gz)
    ld.add_action(declare_use_static_forward_position)
    ld.add_action(declare_use_movee_ctrl_forward_position)
    ld.add_action(declare_use_movee_ctrl_joint_state)
    ld.add_action(declare_twist_twist_pub_topic)
    # ld.add_action(trajectory_publisher_cmd)
    ld.add_action(control_node)
    ld.add_action(joint_state_broadcaster_spawner)
    ld.add_action(delay_diff_controller_spawner_after_joint_state_broadcaster_spawner)
    ld.add_action(delay_forward_controller_spawner_after_joint_state_broadcaster_spawner)
    
    ld.add_action(static_forward_position_publisher)
    ld.add_action(node_forward_position_publisher)
    ld.add_action(node_joint_state_publisher)

    
    # ld.add_action(load_joint_trajectory_controller)
    # ld.add_action(load_diff_drive_base_controller)
    # trajectory_publisher_node,
  

    return ld

