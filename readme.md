# Stack "movee_stack"

This is the stack for the Kythera Movee bipedal wheeled robot with a URDF and associated controllers as well as a gazebo simulation in ROS2


## Demonstration

![Demo of RViz](media/rviz_gui.gif)


## Repository Structure
This project has been organized in stacks for better modularity and reusability

  
```
	├──movee_description/ 				#contains the urdf description and and assets for robot representation in sims and ROS
  	│	├──launch/
  	│	│	└──basic.launch.py 		#launch rviz visualization of the movee robot
  	│	├──meshes/ 				#contains the stl files for acurate rendering
  	│	└──urdf/				#contains the URDF description files of the movee_robot
  	├──movee_gazebo/ 				#contains the aditional elements for Gazebo simulation
  	│	├──worlds/ 				#basic Gazebo worlds for demonstrations
	│	│ 	└──movee.world			#a basic Gazebo world with a coffee table
  	│	└──launch/
	│		├──spawn_movee_gz_sim.launch 		#spawn the movee_robot in Gazebo Classic
	│		└──gz_sim.launch		#spawn the movee_robot in Gazebo simulation (ex Ignition)
  	├──movee_control/ 				#ros controllers for robot operation in the ROS eco-system
	│	├──movee_control/			#variouses nodes
  	│	│	└──joint_animation.py		#joints animation publisher to check movment
  	│	├──config/
  	│	│	└──movee_control.yaml 		#movee controllers descriptions
  	│	└──launch/
  	│		└──movee_control.launch 		#launch file to spawn the controllers
  	├──media/  					#miscelaneous material : videos, tutos, etc...
```


## How to use

### Prepare your work environment

* Option A : Deploy on your Linux (Ubuntu 22.04 recommended)

[Install ROS2](https://docs.ros.org/en/rolling/Installation/Ubuntu-Install-Debians.html), in version **Full-Desktop**, install Gazebo and configure your [work environment](https://docs.ros.org/en/rolling/Tutorials/Configuring-ROS2-Environment.html)  
(This code has been tested on ROS2 Rolling Gazebo Sim (ex Ignition) Fortress. 

Make sure you have the necessary dependencies by typing in the terminal :  
	```
	sudo apt install ros-rolling-xacro -y; sudo apt install ros-rolling-joint-state-publisher-gui -y; sudo apt install ros-rolling-ros2-control -y; sudo apt install ros-rolling-ros2-controllers -y; sudo apt install ros-rolling-gazebo-ros2-control -y
	```

Also add in your workspace the package [twist_stamper](https://github.com/joshnewans/twist_stamper)

### Run the stack

1. Clone this repository in you colcon workspace:   
	`colcon_ws/src`

2. Open a terminal in your catkin_ws:  
	`colcon build`
	
3. Launch the demo for RViz:  
	`ros2 launch movee_description demo.launch.py use_joint_state_pub:=True use_robot_state_pub:=True`  

Feel free to move the robot using the GUI sliders

4.  Launch the robot in Gazebo Sim(Ex ignition)]   

- 1. Launch  Gazebo and instantiate the robot + controllers :  
	`ros2 launch movee_bringup movee_bringup.launch.py use_rviz:=True use_controller:=True` 
By default, no joint controls are sent to the robot, so that it can not stand. 
You can use parameter `use_static_forward_position:=True` to generate static commands for the robot to stay straight. Or you can use `use_movee_ctrl_forward_position:=True` to interpret control commands directly from the [movee_teleop](https://gitlab.com/kytheralab/applications/teleop_joy_cmd_vel/-/blob/main/launch/movee_teleop.launch.py?ref_type=heads) node though the forward_position_command_publisher_node.

(There is still a synchronization issue on node_controller launch, if the controller_load fails, try restarting the command or rebooting the computer)

- 2. Drive the robot with 

`ros2 run teleop_twist_keyboard teleop_twist_keyboard -r /cmd_vel:=/diff_drive_base_controller/cmd_vel_unstamped`   
or with joystick from [joy_teleop](https://gitlab.com/kytheralab/applications/teleop_joy_cmd_vel) repo :   
`ros2 launch teleop_joy_cmd_vel teleop.launch.py pub_topic:=/diff_drive_base_controller/cmd_vel_unstamped`   
or   
`ros2 launch teleop_joy_cmd_vel movee_teleop.launch.py joy_config:='ps4' joy_device_id:=0 pub_topic:=/movee_ctrl_topic`

	
	
## The robot

![Demo of RViz](media/movee.png)

## Example in a world
![Demo of Crawlee in TRR](media/movee_nose_dive.gif)
![Demo of Gazebo](media/run.gif)

## Troubleshoot

If you have error messages, you might need to install aditionnal packages
```
sudo apt install ros-rolling-gazebo-ros
sudo apt install ros-rolling-joint-state-publisher-gui
sudo apt install ros-rolling-joint-state-publisher
sudo apt install ros-rolling-ros2-controllers-test-nodes

```


## ToDo

* Fix Synchonization issue of Controller at startup
* Improve simulation realism

	
## Ressources

0. [A guide for ROS2 and Gazebo](https://automaticaddison.com/how-to-simulate-a-robot-using-gazebo-and-ros-2/)
1. [Use XACRO in ROS 2](https://answers.ros.org/question/361623/ros2-robot_state_publisher-xacro-python-launch/)
2. [Tutorial to load URDF in ROS2](https://github.com/olmerg/lesson_urdf)[and the related video](https://www.youtube.com/watch?v=IfpzNFKnkH0)
3. [ROSControl Documentation](https://ros-controls.github.io/control.ros.org/ros2_controllers/doc/controllers_index.html)
4. [A Tutorial on ROS_COntrol in C++](https://jeffzzq.medium.com/designing-a-ros2-robot-7c31a62c535a)


## License

Tbd
